const express = require("express");
const cors = require("cors");
const { socketController } = require("../sockets/controller");
class Server {
  //vamos a declarar las propiedades en este constructor

  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    //creamos una nueva propiedad de server para crearla con socket.io
    //primero importamos el http que viene de node y usamos la funcion createServer y le pasamos
    //nuestra app de express
    this.server = require("http").createServer(this.app);
    //este this.io => hace referencia a nuestro servidor de socket
    //ahora vamos a crear la propiedad io para ello importamos la libreria socket.io y le mandamos
    //la prpiedad server que creamos antes que nos servira para levatanr el backend
    //io=> es toda la informacion de los sockets conectados, nos permite mandar mensajes a todos los usuarios conectados
    //ademas de tener toda la informacion de los clientes conectados
    this.io = require("socket.io")(this.server);
    this.paths = {};
    //Middleware => funciones que van a añadir otra funcionalidad a mi webserver
    //funcion que siempre se ejecutara cuando levantemos el servidor
    this.middlewares();
    //Rutas de mi aplicacion
    this.routes();
    //Eventos de sockects
    this.sockets();
  }
  middlewares() {
    //cors
    this.app.use(cors());
    //lectura y parceo del middleware
    //debemos configurar para indicarle a express que debe recibir informacion a traves del body en formato json o xml
    //para ello usamos la funcion json de express
    this.app.use(express.json());
    //directorio publico
    this.app.use(express.static("public"));
  }
  routes() {
    //creamos un middleware al cual le colocaremos ciertas rutas es decir cuando
    //pasemos ciertas rutas aqui lo vamos a cargar a esta middleware
    // this.app.use(this.paths.auth, require("../routers/auth"));
  }
  sockets() {
    //para recibir los eventos usamos el metodo on que recibe como primer parametro el nombre del evento
    //este es un evento con nombre connection este se dispara cuando un cliente se conecta
    //para ello se tiene que llamar en el socket-client
    //estos eventos se muestran en el lado del servidor
    //VER SOCKET-CLIENT
    //como segundo parametro recibe un callback, este callback recibe un socket o la coneccion del cliente
    //qioen es el encargado de ejecutar los eventos del lado del servidor
    //VER EN LA CARPETA SOCKETS EN EL ARCHIVO CONTROLLER ESTA FUNCION PARA ENTENDER MEJOR
    this.io.on("connection", socketController);
  }
  listen() {
    //usamos el server de socket.io
    //http://localhost:8080/socket.io/socket.io.js probar
    this.server.listen(this.port, () => {
      console.log("Servidor corriendo en puerto", this.port);
    });
  }
}
module.exports = Server;
