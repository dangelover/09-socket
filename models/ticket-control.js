const path = require("path");
const fs = require("fs");
//clase para manejar los tickets
class Ticket {
  //vamos a recibir en el constructor el numero y el esctritorio
  constructor(numero, escritorio) {
    //numero de ticket
    this.numero = numero;
    //escrito al cual pertenece
    this.escritorio = escritorio;
  }
}
class TicketControl {
  //vamos a colocar las propiedades dentro del constructor
  constructor() {
    //ultimo ticket, cuando llamemos al siguiente ticket este aumentara su valor
    this.ultimo = 0;
    //vamos a obtener la fecha de hoy usando la funcion getDate
    //esto servira para comparar la fecha de hoy con el que tenemos en la base de datos
    this.hoy = new Date().getDate();
    //arreglo de tickets pendientes
    this.tickets = [];
    //arreglo de los ultimos 4 tickets
    this.ultimos4 = [];
    this.init();
  }
  //cuando llamemos a este toJson va a generar el siguente objeto
  get toJson() {
    return {
      ultimo: this.ultimo,
      hoy: this.hoy,
      tickets: this.tickets,
      ultimos4: this.ultimos4,
    };
  }
  //metodo para inicializar la clase
  init() {
    //cuando llamemos el init tenemos que leer el archivo json
    //para ello primero importamos el json y node lo va a transformar a un objeto de js
    const { hoy, tickets, ultimo, ultimos4 } = require("../db/data.json");
    // console.log(data);
    //vamos a evaluar si el dia de la data es igual al dia de hoy, si es igual entonces vamos a leer la informacion de la data
    //para cargar estas propiedades
    if (hoy === this.hoy) {
      //si es asi entonces estamos trabajando en el mismo dia y solo estamos recargando el servidor
      this.tickets = tickets;
      this.ultimo = ultimo;
      this.ultimos4 = ultimos4;
    } else {
      //si no son iguales entonces es otro dia y tenemos que devolver los valores iniciales y guardar
      this.guardarDB();
    }
  }
  guardarDB() {
    //primero creamos nuestro path donde se guardara la data
    const dbPath = path.join(__dirname, "../db/data.json");
    //usando la libreria fs vamos a grabar usando el writeFileSync
    //este pide como primer parametro el path y el nombre del archivo luego
    //como segundo parametro el archivo que queremos guardar
    //pero este metodo solo acepta string, para ello convertimos nuestro objeto en un string usando stringify
    fs.writeFileSync(dbPath, JSON.stringify(this.toJson));
  }
  siguiente() {
    this.ultimo += 1;
    //creamos una nueva instancia de la clase ticket y le pasamos el ultimo ticket y null para el escritorio
    const ticket = new Ticket(this.ultimo, null);
    //agregamos este ticket al arreglo de tickets
    this.tickets.push(ticket);
    this.guardarDB();
    //este metodo va a retornar el numero del ultimo ticket
    return "Ticket" + ticket.numero;
  }
  atenderTicket(escritorio) {
    //no tenemos tickets
    if (this.tickets.length === 0) {
      return null;
    }
    //vamos a remover el ticket atendido es decir el primer ticket del arreglo para ello usamos shif
    //usamos shift para quitar ese ticket del arreglo y asignarlo a la variable ticket
    const ticket = this.tickets.shift();
    // a este ticket le asignamos el escritorio que recibimos para antenderlo
    ticket.escritorio = escritorio;
    //ahora usando el unshift vamos a agregar al principio del arreglo el ticket que sacamos
    this.ultimos4.unshift(ticket);
    if (this.ultimos4.length > 4) {
      //comprobamos si la cantidad de elementos del arreglo ultimos4 es mayor a 4 entoncesv vamos a borrar el ultimo
      //para ello usamos splice
      //-1=> empezar desde el ultimo
      //1 => cortar solo uno
      this.ultimos4.splice(-1, 1);
    }
    this.guardarDB();
    return ticket;
  }
}
module.exports = TicketControl;
