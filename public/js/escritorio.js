//REFERENCIAS HTML
const lblEscritorio = document.querySelector("h1");
const btnAtender = document.querySelector("button");
const lblTicket = document.querySelector("small");
const divAlerta = document.querySelector(".alert");
const lblPendientes = document.querySelector("#lblPendientes");
//VAMOS A LEER LOS PARAMETROS DE LA ULR
//PARA ELLO USAMOS EL METODO window.location.search DE LA LIBRERIA URLSearchParams
const serchParams = new URLSearchParams(window.location.search);
//si en los paremetros no existe escritorio
if (!serchParams.has("escritorio")) {
  window.location = "index.html";
  throw new Error("El escritorio es obligatorio");
}
const escritorio = serchParams.get("escritorio");
// console.log({ escritorio });
lblEscritorio.innerText = escritorio;
divAlerta.style.display = "none";
const socket = io();
//vamos usar listener que van a escuchar cambios o eventos
//el on es un metodo del socket  para escuchar un evento
//estos eventos se mostraran en el cliente
//connect => es un evento que se dispara cuando haya una conexion
socket.on("connect", () => {
  console.log("conectado");
  //cuando el servidor de socket esta conectado el boton estara activado
  btnAtender.disabled = false;
});
//disconnect => evento que se dispara cuando se desconecta
socket.on("disconnect", () => {
  //cuando servidor de socket esta conectado  el boton estara desactivado
  btnAtender.disabled = true;
});
socket.on("ultimo-ticket", (ultimo) => {
  //   console.log(ultimo);
  lblTicket.innerText = "Nadie";
});
socket.on("tickets-pendientes", (ticketsPendientes) => {
  // console.log(payload);
  if (ticketsPendientes === 0) {
    lblPendientes.style.display = "none";
  } else {
    lblPendientes.style.display = "";
    lblPendientes.innerText = ticketsPendientes;
  }
});
btnAtender.addEventListener("click", () => {
  //en el callback vamos a recibir el ticket a atender y el ok que nos indica si esta bien o mal
  //y el msg donde indica el tipo de error
  //en el payload de este evento vamos a enviar el escritorio
  socket.emit("atender-ticket", { escritorio }, ({ ok, ticket, msg }) => {
    // console.log(payload);
    //si el ok es false quiere decir que hay un error
    if (!ok) {
      lblTicket.innerText = "Nadie";
      return (divAlerta.style.display = "");
    }
    //si no hay ningun error entonces si tenemos un ticket
    lblTicket.innerText = "Ticket " + ticket.numero;
  });
});
