//Referencias HTML
const lblNuevoTicket = document.querySelector("#lblNuevoTicket");
const btnCrear = document.querySelector("button");
const socket = io();
//vamos usar listener que van a escuchar cambios o eventos
//el on es un metodo del socket  para escuchar un evento
//estos eventos se mostraran en el cliente
//connect => es un evento que se dispara cuando haya una conexion
socket.on("connect", () => {
  console.log("conectado");
  //cuando el servidor de socket esta conectado el boton estara activado
  btnCrear.disabled = false;
});
//disconnect => evento que se dispara cuando se desconecta
socket.on("disconnect", () => {
  //cuando servidor de socket esta conectado  el boton estara desactivado
  btnCrear.disabled = true;
});
socket.on("ultimo-ticket", (ultimo) => {
  //   console.log(ultimo);
  lblNuevoTicket.innerText = "Ticket " + ultimo;
});
btnCrear.addEventListener("click", () => {
  //   console.log(mensaje);
  //haciendo uso del emit podemos emitir un evento
  //en nuestro server vamos a recibir este evento
  //REVISAR SERVER
  //recibe como parametro el nombre del evento
  //recibe como segundo parametro el valor que queremos enviar
  //como tercer parametro podemos colocar un callback que va a recibir cualquier valor del servidor
  //este callback es llamado desde el servidor y ejecutado aqui en el cliente
  socket.emit("siguiente-ticket", null, (ticket) => {
    // console.log("desde el server", ticket);
    lblNuevoTicket.innerText = ticket;
  });
});
