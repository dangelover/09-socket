const TicketControl = require("../models/ticket-control");

//inicalizamos nuestra clase
const ticketControl = new TicketControl();
const socketController = (socket) => {
  socket.emit("ultimo-ticket", ticketControl.ultimo);
  //tickets.pendientes
  socket.emit("estado-actual", ticketControl.ultimos4);
  socket.emit("tickets-pendientes", ticketControl.tickets.length);
  //en el callback lo vamos a utilizar cuando llame el cliente siguiente ticket en el callback le vamos a mandar cual es el
  //ticket que tiene que mostrar en la pantalla
  socket.on("siguiente-ticket", (payload, callback) => {
    //el metodo siguiente retorna el numero del ultimo ticket
    const siguiente = ticketControl.siguiente();
    //en el siguiente esta el numero del ultimo ticket y este vamos a enviar mediante el callback
    callback(siguiente);
    //TODO: Notificar que hay un nuevo ticket pendiente de asignar
    socket.broadcast.emit("tickets-pendientes", ticketControl.tickets.length);
  });
  //vamos a escuchar el evento atender-ticket, para atender el ultimo ticket
  //y recibimos el payload que tiene el escritorio
  socket.on("atender-ticket", ({ escritorio }, callback) => {
    // console.log(escritorio);
    if (!escritorio) {
      //si es que no envian el escritorio entonces retornamos un callback
      //con un mensaje
      return callback({
        ok: false,
        msg: "El escritorio es obligatorio",
      });
    }
    //obtenemos el ticket que queremos atender
    //para ello usamos nuestro metodo atenderTicket
    //este retona un null si es que no hay tickets en la cola
    const ticket = ticketControl.atenderTicket(escritorio);
    //cuando asignemos un ticket a un escritorio, es necesario cargar los ultimos 4 tickets
    //ya que los ultimos 4 cambiaron y tienen un nuevo escritorio y es necesario notificar
    socket.broadcast.emit("estado-actual", ticketControl.ultimos4);
    //usamos el primer emit para que se emita en el escritorio donde se atiende
    socket.emit("tickets-pendientes", ticketControl.tickets.length);
    //ahora usamos el emit mas el broadcast para que las demas pantallas tambien lo vean
    socket.broadcast.emit("tickets-pendientes", ticketControl.tickets.length);
    if (!ticket) {
      callback({
        ok: false,
        msg: "Ya no hay tickets pendientes",
      });
    } else {
      //pero si existe tickets en la cola retornara el primer ticket
      callback({
        ok: true,
        ticket,
      });
    }
  });
};

module.exports = {
  socketController,
};
